<?php

namespace AOD\TheCats;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use RuntimeException;
use function GuzzleHttp\json_decode as guzzle_json_decode;

class TheCats
{
    const API_URL = 'https://api.thecatapi.com';
    const API_VERSION = 'v1';

    const ALLOWED_ORDERS = ['desc', 'asc', 'rand'];
    const ALLOWED_MIME_TYPES = ['jpg', 'png', 'gif'];

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var Client
     */
    protected $client;

    protected $pagination = 0;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->apiKey = getenv('THE_CAT_API_KEY');
    }

    /**
     * Returns a list of paginated images from the cat API
     * @param array $query
     * @return array|null
     */
    public function images($query = [])
    {
        try {
            $query = array_parse_defaults($query, [
                'order' => 'desc',
                'page' => 1,
                'limit' => 10
            ]);

            // The API only allows an enumerable set of strings for mime type. Remove it if it's not allowed
            if (isset($query['mime_types']) && !in_array($query['mime_types'], self::ALLOWED_MIME_TYPES)) {
                unset($query['mime_types']);
            }

            // The api only allows certain orders, make sure we pass an acceptable option
            if (!in_array($query['order'], self::ALLOWED_ORDERS)) {
                $query['order'] = 'desc';
            }

            $response = $this->client->get(
                $this->_getRequestUrl('images/search'),
                $this->_getRequestOptions(compact('query'))
            );

            // This method is paginated, add the API's pagination results
            $this->_setPagination([
                'total' => (int) $response->getHeaderLine('Pagination-Count'),
                'limit' => (int) $response->getHeaderLine('Pagination-Limit'),
                'page' => (int) $response->getHeaderLine('Pagination-Page')
            ]);

            return guzzle_json_decode($response->getBody());
        } catch (Exception $exception) {
            // We don't want our app to break if the API responds with an error
            return null;
        }
    }

    /**
     * Returns a list of categories from the cat API
     * @return array|null
     */
    public function categories()
    {
        try {
            $response = $this->client->get($this->_getRequestUrl('categories'));

            return guzzle_json_decode($response->getBody());
        } catch (Exception $exception) {
            // We don't want our app to break if the API responds with an error
            return null;
        }
    }

    /**
     * Returns a list of favorites attached to a user
     * @param int $user_id
     * @return mixed|null
     */
    public function favorites($user_id)
    {
        try {
            $query = [
                'sub_id' => $this->_parseUserId($user_id),
            ];

            $response = $this->client->get(
                $this->_getRequestUrl('favourites'),
                $this->_getRequestOptions(compact('query'))
            );

            return guzzle_json_decode($response->getBody());
        } catch (Exception $exception) {
            // We don't want our app to break if the API responds with an error
            return null;
        }
    }

    /**
     * Appends a favorite to a user
     * @param int $user_id
     * @param int $image_id
     * @return mixed|null
     */
    public function addFavorite($user_id, $image_id)
    {
        try {
            $response = $this->client->post(
                $this->_getRequestUrl('favourites'),
                $this->_getRequestOptions([
                    'json' => [
                        'sub_id' => $this->_parseUserId($user_id),
                        'image_id' => $image_id
                    ]
                ])
            );

            return guzzle_json_decode($response->getBody());
        } catch (Exception $exception) {
            // This will reply with an error on duplicates, no need to re-save ours
            return null;
        }
    }

    /**
     * @param int $favorite_id
     * @return mixed|null
     */
    public function removeFavorite($favorite_id)
    {
        try {
            $response = $this->client->delete(
                $this->_getRequestUrl('favourites/' . $favorite_id),
                $this->_getRequestOptions()
            );

            return guzzle_json_decode($response->getBody());
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * @return array
     */
    public function getMimeTypes()
    {
        return self::ALLOWED_MIME_TYPES;
    }

    /**
     * @return int
     */
    public function getPagination()
    {
        return $this->pagination;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Private and Protected

    /**
     * The api does not like integers as ids, it prefers GUIDs. we're going to use a string
     * @param int $id
     * @return string
     */
    private function _parseUserId($id)
    {
        return sprintf('user-%s', $id);
    }

    /**
     * Returns request options for the client
     * @param array $options
     * @return array
     */
    private function _getRequestOptions($options = [])
    {
        return array_parse_defaults($options, [
            'headers' => [
                'x-api-key' => $this->apiKey,
            ]
        ]);
    }

    /**
     * Returns a properly constructed URL for the API
     * @param null|string $path
     * @return string
     */
    private function _getRequestUrl($path = null)
    {
        if (empty($path)) {
            throw new RuntimeException('Missing url path');
        }

        return sprintf('%s/%s/%s', self::API_URL, self::API_VERSION, $path);
    }

    /**
     * @param array $pagination
     */
    protected function _setPagination($pagination)
    {
        $this->pagination = $pagination;
        $this->pagination['pages'] = $this->pagination['total'] > 0
            ? floor($this->pagination['total'] / $this->pagination['limit'])
            : 0;
    }
}
