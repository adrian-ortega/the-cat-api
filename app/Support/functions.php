<?php

use Slim\Psr7\Request;
use Slim\Routing\RouteParser;

if (!function_exists('site_url')) {
    /**
     * Returns the site url along with an extra path
     * @param string $path
     * @return string
     */
    function site_url($path = '') {
        $isSecure = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443);
        $protocol = $isSecure ? "https://" : "http://";
        $base_url = $protocol . $_SERVER['HTTP_HOST'];

        return $base_url . (empty($path) ? '' : '/' . ltrim($path, '/'));
    }
}

if(!function_exists('url_for')) {
    /**
     * Returns a fully qualified url for a route registered in the app
     * @param string $routeName
     * @param array $data
     * @param array $queryParams
     * @return mixed
     */
    function url_for($routeName, $data = [], $queryParams = []) {
        global $app;
        $parser = $app->getContainer()->get(RouteParser::class);
        $request = $app->getContainer()->get(Request::class);

        return $parser->fullUrlFor($request->getUri(), $routeName, $data, $queryParams);
    }
}
