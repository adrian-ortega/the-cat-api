<?php

namespace AOD\Support\Traits;

use AOD\TheCats\TheCats;
use League\Container\Container;
use Psr\Container\ContainerInterface;

/**
 * Trait HasCats
 * @package AOD\Support\Traits
 * @method Container|ContainerInterface getContainer()
 */
trait ControllerHasCats
{
    /**
     * Returns the cats api class from the container
     * @return TheCats
     */
    protected function cats()
    {
        return $this->getContainer()->get(TheCats::class);
    }
}
