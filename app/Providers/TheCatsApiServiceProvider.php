<?php

namespace AOD\Providers;

use AOD\TheCats\TheCats;
use GuzzleHttp\Client;
use League\Container\ServiceProvider\AbstractServiceProvider;

class TheCatsApiServiceProvider extends AbstractServiceProvider
{
    protected $provides = [
        TheCats::class
    ];

    public function register()
    {
        $this->getContainer()->share(TheCats::class, function() {
            return new TheCats(new Client([
                // @TODO remove this when pushing to SSL enabled production environment
                'verify' => false
            ]));
        });
    }
}
