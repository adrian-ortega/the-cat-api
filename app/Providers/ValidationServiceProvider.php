<?php

namespace AOD\Providers;

use AOD\Models\User;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use League\Container\ServiceProvider\AbstractServiceProvider;
use League\Container\ServiceProvider\BootableServiceProviderInterface;
use Valitron\Validator;

class ValidationServiceProvider extends AbstractServiceProvider implements BootableServiceProviderInterface
{
    public function boot()
    {
        Validator::addRule('emailIsUnique', function ($field, $value, $params, $fields) {

            $sentinelUser = Sentinel::check() ? Sentinel::check() : false;
            $user = User::where('email', $value)
                ->where('email', '!=', $sentinelUser ? $sentinelUser->email : '')
                ->first();

            return $user ? false : true;
        }, 'is already in use');

        Validator::addRule('currentPassword', function ($field, $value, $params, $fields) {
            return Sentinel::getUserRepository()->validateCredentials(
                Sentinel::check(),
                ['password' => $value]
            );
        }, 'is wrong');
    }

    public function register()
    {
        // Registers nothing
    }
}
