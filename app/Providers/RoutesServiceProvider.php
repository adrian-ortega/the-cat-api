<?php

namespace AOD\Providers;

use League\Container\ServiceProvider\AbstractServiceProvider;
use Slim\App;
use Slim\Routing\RouteParser;

class RoutesServiceProvider extends AbstractServiceProvider
{
    protected $app;

    protected $provides = [
        RouteParser::class
    ];

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function register()
    {
        $parser = $this->app->getRouteCollector()->getRouteParser();

        $this->getContainer()->share(RouteParser::class, function() use ($parser) {
            return $parser;
        });
    }
}
