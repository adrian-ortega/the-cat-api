<?php

namespace AOD\Providers;

use League\Container\ServiceProvider\AbstractServiceProvider;
use Slim\Views\Twig;

class TwigViewServiceProvider extends AbstractServiceProvider
{
    protected $provides = [
        Twig::class
    ];

    /**
     * @inheritDoc
     */
    public function register()
    {
        $this->getContainer()->share(Twig::class, function() {
            return Twig::create(ABS_PATH . 'resources' . DS . 'views' . DS, [
                // @TODO change this with an env flag
                'cache' => false
            ]);
        });
    }
}
