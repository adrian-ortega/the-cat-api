<?php

namespace AOD\Exceptions;

use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Throwable;

class ExceptionHandler
{
    public function __invoke(Request $request, Throwable $exception)
    {
        if ($exception instanceof ValidationException) {

            // This is essentially a copy of the respondWithJson method
            // in the AbstractApiController

            $response = new Response();
            $response->getBody()->write($json = json_encode([
                'message' => 'Validation errors',
                'errors' => $exception->getErrors()
            ]));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(403);
        }

        throw $exception;
    }
}
