<?php

namespace AOD\Exceptions;

use Exception;

class ValidationException extends Exception
{
    /**
     * @var array
     */
    protected $errors;

    /**
     * @var string
     */
    protected $path;

    public function __construct(array $errors, $path)
    {
        $this->errors = $errors;
        $this->path = $path;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}
