<?php

namespace AOD\Auth;

use Carbon\Carbon;
use Exception;
use Firebase\JWT\JWT as FirebaseJWT;

class JWT
{
    /**
     * @var string
     */
    protected $hash_algorithm = 'HS256';

    /**
     * Lifespan in minutes
     * @var int
     */
    protected $lifespan = 120;

    public function __construct()
    {
        $this->lifespan = (60 * 24) * 30;
    }

    /**
     * Returns a generated token to be used by an authenticated user
     *
     * @param int $user_id
     *
     * @return string
     * @throws Exception
     */
    public static function token($user_id) {
        $jwt = new static;
        return FirebaseJWT::encode(
            $jwt->generateTokenPayload($user_id),
            $jwt->getSecret(),
            $jwt->getHashAlgorithm()
        );
    }

    /**
     * Returns the algorithm used
     * @return string
     */
    protected function getHashAlgorithm() {
        return $this->hash_algorithm;
    }

    /**
     * Generates a token payload to be encoded
     *
     * @param $user_id
     *
     * @return array
     * @throws Exception
     */
    protected function generateTokenPayload($user_id) {
        return [
            'iss' => site_url(),
            'iat' => $iat = $this->getInitiatedAtTime(),
            'exp' => $this->getExpiryTime(),
            'user' => $user_id,
        ];
    }

    /**
     * Returns the secret code used
     * @return mixed
     */
    protected function getSecret()
    {
        return getenv('JWT_SECRET');
    }

    /**
     * @return int
     * @throws Exception
     */
    protected function getInitiatedAtTime()
    {
        return (new Carbon())->timestamp;
    }

    /**
     * @return int
     * @throws Exception
     */
    protected function getExpiryTime()
    {
        return (new Carbon())->addMinutes($this->lifespan)->timestamp;
    }
}
