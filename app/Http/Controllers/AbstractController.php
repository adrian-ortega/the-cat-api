<?php

namespace AOD\Http\Controllers;

use AOD\Exceptions\ValidationException;
use League\Container\ContainerAwareInterface;
use League\Container\ContainerAwareTrait;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\Message;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Slim\Views\Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Valitron\Validator;

abstract class AbstractController implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var Twig
     */
    protected $view;

    /**
     * AbstractController constructor.
     * @param Twig $twig
     */
    public function __construct(Twig $twig)
    {
        $this->view = $twig;
    }

    /**
     * @param string $routeName
     * @param int $status
     * @return MessageInterface|ResponseInterface|Message|Response
     */
    public function redirect($routeName, $status = 301)
    {
        return (new Response())
            ->withStatus($status)
            ->withHeader('Location', url_for($routeName));
    }

    /**
     * @param ResponseInterface $response
     * @param $template
     * @param array $data
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function render(ResponseInterface $response, $template, $data = [])
    {
        $template = $this->_parseTemplate($template);
        return $this->view->render($response, $template, $data);
    }

    /**
     * @param Request $request
     * @param array $rules
     * @return array|object|null
     * @throws ValidationException
     */
    public function validate(Request $request, array $rules = [])
    {
        $validator = new Validator(
            $params = in_array($request->getMethod(), ['GET', 'DELETE'])
                ? $request->getQueryParams()
                : $request->getParsedBody()
        );

        $validator->mapFieldsRules($rules);

        if (!$validator->validate()) {
            throw new ValidationException(
                $validator->errors(),
                $request->getUri()->getPath()
            );
        }

        return $params;
    }

    /**
     * Allows dot notation for template paths like 'home.part'
     * @param string $template
     * @return string
     */
    private function _parseTemplate($template)
    {
        // Make sure to remove any references to twig
        $template = str_replace('.twig', '', $template);

        // Convert to an array for dot notation parsing below
        $parts = (array) $template;

        // Check if we have a subdirectory with dot notation
        if(strpos($template, '.') !== false) {
            $parts = array_map('trim', explode('.', $template));
        }

        $template = '';

        // Parse subdirectories, if they exist
        foreach ($parts as $part) {
            $template .= trim($part) . '/';
        }

        return rtrim($template, '/') . '.twig';
    }
}
