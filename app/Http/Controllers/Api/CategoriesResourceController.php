<?php

namespace AOD\Http\Controllers\Api;

use AOD\Http\Transformers\CategoryTransformer;
use AOD\Support\Traits\ControllerHasCats;
use Psr\Http\Message\MessageInterface;
use Slim\Psr7\Response;

class CategoriesResourceController extends AbstractApiController
{
    use ControllerHasCats;

    /**
     * @return MessageInterface|Response
     */
    public function index()
    {
        // Attempt to get categories from the API, set to empty array if null
        // @TODO update to send an 'offline' response
        if(!$data = $this->cats()->categories()) {
            $data = [];
        }

        return $this->collectionResponse($data, new CategoryTransformer());
    }
}
