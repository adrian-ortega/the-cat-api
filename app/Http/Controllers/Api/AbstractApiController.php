<?php

namespace AOD\Http\Controllers\Api;

use AOD\Http\Controllers\AbstractController;
use Exception;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\ResourceAbstract;
use League\Fractal\Scope as FractalScope;
use League\Fractal\TransformerAbstract;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use Slim\Psr7\Message;
use Slim\Psr7\Response;

abstract class AbstractApiController extends AbstractController
{
    /**
     * @param Response $response
     * @return MessageInterface|Message
     */
    protected function isNotFound(Response $response = null)
    {
        if(!$response) {
            $response = new Response();
        }

        return $response->withHeader('Location', url_for('api-not-found'));
    }

    protected function isForbidden($data = null, Response $response = null)
    {
        if(!$response) {
            $response = new Response();
        }

        if(!is_array($data)) {
            $data = [
                'message' => $data ? $data : 'Forbidden',
                'code' => 403
            ];
        }

        return $this->respondWithJson($response, $data, 403);
    }

    /**
     * @param Response $response
     * @param $data
     * @param null $status
     * @param int $encodingOptions
     * @return Response|MessageInterface
     */
    protected function respondWithJson(Response $response, $data = [], $status = null, $encodingOptions = 0)
    {
        try {
            $response->getBody()->write($json = json_encode($data, $encodingOptions));

            if($json === false) {
                throw new RuntimeException(json_last_error_msg(), json_last_error());
            }

            $responseWithJson = $response->withHeader('Content-Type', 'application/json');

            return $status ? $responseWithJson->withStatus($status) : $responseWithJson;
        } catch (Exception $exception) {
            return $this->respondWithJson(new Response(), [
                'message' => 'Something went wrong',
                'code' => 500
            ], 500);
        }
    }

    /**
     * @param ResourceAbstract $resource
     * @param null $resourceIncludes
     * @return FractalScope
     */
    protected function getFractalResponse(ResourceAbstract $resource, $resourceIncludes = null)
    {
        $fractal = new Manager();

        if($resourceIncludes) {
            $fractal->parseIncludes($resourceIncludes);
        }

        return $fractal->createData($resource);
    }

    /**
     * Converts the Resource response to JSON using Fractal
     * @param ResourceAbstract $resource
     * @param null $resourceIncludes
     * @param int $status
     * @return ResponseInterface|Response
     */
    protected function resourceResponse(ResourceAbstract $resource, $resourceIncludes = null, $status = 200)
    {
        return $this->respondWithJson(
            new Response(),
            $this->getFractalResponse($resource, $resourceIncludes)->toArray()
        )->withStatus($status);
    }

    /**
     * @param array $collection
     * @param TransformerAbstract $transformer
     * @param null $resourceIncludes
     * @param null $meta
     * @param int $status
     * @return ResponseInterface|Response
     */
    protected function collectionResponse(
        $collection,
        TransformerAbstract $transformer,
        $resourceIncludes = null,
        $meta = null,
        $status = 200
    )
    {
        $resource = new Collection($collection, $transformer);

        if($meta) {
            $resource->setMeta($meta);
        }

        return $this->resourceResponse($resource, $resourceIncludes, $status);
    }

    /**
     * Single resource item
     * @param mixed $item
     * @param TransformerAbstract $transformer
     * @param null $resourceIncludes
     * @param null $meta
     * @param int $status
     * @return ResponseInterface|Response
     */
    protected function itemResponse(
        $item,
        TransformerAbstract $transformer,
        $resourceIncludes = null,
        $meta = null,
        $status = 200
    )
    {
        $resource = new Item($item, $transformer);

        if($meta) {
            $resource->setMeta($meta);
        }

        return $this->resourceResponse($resource, $resourceIncludes, $status);
    }
}
