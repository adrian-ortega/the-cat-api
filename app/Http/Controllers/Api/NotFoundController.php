<?php

namespace AOD\Http\Controllers\Api;

use Slim\Psr7\Request;
use Slim\Psr7\Response;

class NotFoundController extends AbstractApiController
{
    public function __invoke(Request $request, Response $response)
    {
        return $this->respondWithJson($response, [
            'message' => 'Page Not found',
            'code' => 404
        ], 404);
    }
}
