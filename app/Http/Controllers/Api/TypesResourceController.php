<?php

namespace AOD\Http\Controllers\Api;

use AOD\Http\Transformers\MimeTypeTransformer;
use AOD\Support\Traits\ControllerHasCats;
use Psr\Http\Message\MessageInterface;
use Slim\Psr7\Response;

class TypesResourceController extends AbstractApiController
{
    use ControllerHasCats;

    /**
     * @return MessageInterface|Response
     */
    public function index()
    {
        return $this->collectionResponse(
            $this->cats()->getMimeTypes(),
            new MimeTypeTransformer()
        );
    }
}
