<?php

namespace AOD\Http\Controllers\Api;

use Slim\Psr7\Request;
use Slim\Psr7\Response;

class ForbiddenController extends AbstractApiController
{
    public function __invoke(Request $request, Response $response)
    {
        return $this->respondWithJson($response, [
            'message' => 'Forbidden',
            'code' => 403
        ], 403);
    }
}
