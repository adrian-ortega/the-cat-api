<?php

namespace AOD\Http\Controllers\Api;

use AOD\Http\Transformers\CatTransformer;
use AOD\Support\Traits\ControllerHasCats;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class CatsResourceController extends AbstractApiController
{
    use ControllerHasCats;

    /**
     * @param Request $request
     * @return MessageInterface|Response
     */
    public function index(Request $request)
    {
        $params = $request->getQueryParams();
        $filters = array_filter([
            'category_ids' => (int) data_get($params, 'category', -1),
            'mime_types' => data_get($params, 'type')
        ], function($i) {
            return $i && $i !== -1;
        });

        // @TODO Verify category and mime type?

        return $this->respondWithCats(
            array_merge($this->getCatPaginationParams($request), $filters)
        );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return MessageInterface|Response
     */
    public function byCategory(Request $request, Response $response, $args)
    {
        $category = (int) data_get($args, 'category');

        if(!in_array($category, $this->getCategoryIds())) {
            return $this->isNotFound($response);
        }

        $params = $this->getCatPaginationParams($request);
        $params['category_ids'] = $category;

        return $this->respondWithCats($params);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return MessageInterface|Response
     */
    public function byType(Request $request, Response $response, $args)
    {
        $type = data_get($args, 'type');

        if(!in_array($type, $this->cats()->getMimeTypes())) {
            return $this->isNotFound($response);
        }

        $params = $this->getCatPaginationParams($request);
        $params['mime_types'] = $type;

        return $this->respondWithCats($params);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Private and Protected

    /**
     * Responds with a formatted list of cat data using Fractal
     * @param array $params
     * @return ResponseInterface|Response
     */
    protected function respondWithCats($params)
    {
        $cats = $this->getCatData($params);
        return $this->collectionResponse(
            data_get($cats, 'data'),
            new CatTransformer(),
            null,
            data_get($cats, 'meta')
        );
    }

    /**
     * Returns cat data from the API
     * @param array $params
     * @return array
     */
    protected function getCatData($params = [])
    {
        // attempt to get image data.
        // if for any reason the api breaks (returns null), set the data to an empty array
        // @TODO update to send an 'offline' response
        if(!$data = $this->cats()->images($params)) {
            $data = [];
        }

        $meta = [
            'pagination' => $this->cats()->getPagination()
        ];

        return compact('data', 'meta');
    }
    /**
     * Returns the default pagination query parameters
     * @param Request $request
     * @return array
     */
    protected function getCatPaginationParams(Request $request)
    {
        $params = $request->getQueryParams();

        return [
            'order' => data_get($params, 'order', 'desc'),
            'page' => (int) data_get($params, 'page', 1),
            'limit' => (int) data_get($params, 'limit', 10)
        ];
    }

    /**
     * Returns a list of cat category ids from the API
     * @return array|int[]
     */
    protected function getCategoryIds()
    {
        $categories = $this->cats()->categories();
        return array_map(pluck_value_from_item_by_key('id'), $categories);
    }
}
