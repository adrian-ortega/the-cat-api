<?php

namespace AOD\Http\Controllers\Api\Auth;

use AOD\Auth\JWT;
use AOD\Exceptions\ValidationException;
use AOD\Http\Controllers\Api\AbstractApiController;
use AOD\Http\Transformers\UserTransformer;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Exception;
use Illuminate\Support\Arr;
use Psr\Http\Message\MessageInterface;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class LoginController extends AbstractApiController
{
    public function index()
    {
        return $this->redirect('home');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return MessageInterface|Response
     * @throws ValidationException
     */
    public function action(Request $request, Response $response)
    {
        $data = $this->validate($request, [
            'email' => ['email', 'required'],
            'password' => ['required']
        ]);

        try {
            if (
                !$user = Sentinel::authenticate(
                    Arr::only($data, ['email', 'password']),
                    Arr::has($data, 'persist')
                )
            ) {
                throw new Exception('Incorrect email or password');
            }
        } catch (Exception $e) {
            return $this->isForbidden($e->getMessage());
        }

        $token = JWT::token($user->id);
        return $this->itemResponse($user, new UserTransformer(), ['favorite_count'], compact('token'));
    }
}
