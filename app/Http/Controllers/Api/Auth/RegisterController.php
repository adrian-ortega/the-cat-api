<?php

namespace AOD\Http\Controllers\Api\Auth;

use AOD\Auth\JWT;
use AOD\Http\Controllers\Api\AbstractApiController;
use AOD\Http\Transformers\UserTransformer;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Exception;
use Illuminate\Support\Arr;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class RegisterController extends AbstractApiController
{
    public function index()
    {
        return $this->redirect('home');
    }

    public function action(Request $request, Response $response)
    {
        $data = $this->validate($request, [
            'email' => ['required', 'email' , 'emailIsUnique'],
            'first_name' => ['required'],
            'last_name' => ['required'],
            'password' => ['required']
        ]);

        try {
            $user = Sentinel::register(
                Arr::only($data, ['email', 'first_name', 'last_name', 'password']),
                true
            );

            Sentinel::authenticate(Arr::only($data, ['email', 'password']));
        } catch (Exception $e) {
            return $this->isForbidden($e->getMessage());
        }

        $token = JWT::token($user->id);
        return $this->itemResponse($user, new UserTransformer(), ['favorite_count'], compact('token'));
    }
}
