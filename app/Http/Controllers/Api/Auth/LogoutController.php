<?php

namespace AOD\Http\Controllers\Api\Auth;

use AOD\Http\Controllers\Api\AbstractApiController;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class LogoutController extends AbstractApiController
{
    public function index(Request $request, Response $response)
    {
        Sentinel::logout();

        return $this->respondWithJson($response, [
            'message' => 'Successfully logged out'
        ], 200);
    }
}
