<?php

namespace AOD\Http\Controllers\Api;

use AOD\Auth\JWT;
use AOD\Http\Transformers\CatTransformer;
use AOD\Http\Transformers\UserFavoriteTransformer;
use AOD\Http\Transformers\UserTransformer;
use AOD\Models\UserFavorite;
use AOD\Support\Traits\ControllerHasCats;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class UserController extends AbstractApiController
{
    use ControllerHasCats;

    public function me(Request $request, Response $response)
    {
        $user = $request->getAttribute('user');
        $token = JWT::token($user->id);
        return $this->itemResponse($user, new UserTransformer(), ['favorite_count'], compact('token'));
    }

    public function addFavorite(Request $request, Response $response)
    {
        $params = $this->validate($request, [
            'id' => ['required']
        ]);

        $user = $request->getAttribute('user');
        $cat_id = data_get($params, 'id');

        if($favorite = $this->cats()->addFavorite($user->id, $cat_id)) {
            // We only save favorites to our db if the API says they're not a duplicate
            $user->favorites()->create([
                'cat_id' => $cat_id,
                'favorite_id' => $favorite->id
            ]);
        }

        return $this->collectionResponse($user->favorites, new UserFavoriteTransformer);
    }

    public function favorites (Request $request, Response $response)
    {
        $user = $request->getAttribute('user');

        $favorites = $this->cats()->favorites($user->id);

        $meta = [
            // Don't care about pagination
            'pagination' => [
                'page' => 1,
                'pages' => 1,
                'total' => count($favorites),
                'limit' => 1000
            ]
        ];

        return $this->collectionResponse($favorites, new CatTransformer(), ['favorite_id'], $meta);
    }

    public function removeFavorite(Request $request, Response $response)
    {
        $params = $this->validate($request, [
            'id' => ['required']
        ]);

        $user = $request->getAttribute('user');
        $cat_id = data_get($params, 'id');

        if ($favorite = $user->favorites()->where('cat_id', $cat_id)->first()) {
            if($this->cats()->removeFavorite($favorite->favorite_id)) {
                UserFavorite::where('favorite_id', $favorite->favorite_id)->delete();
            }
        }

        return $this->collectionResponse($user->favorites, new UserFavoriteTransformer);
    }
}
