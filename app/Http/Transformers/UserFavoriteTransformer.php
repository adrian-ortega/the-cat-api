<?php

namespace AOD\Http\Transformers;

use AOD\Models\UserFavorite;

class UserFavoriteTransformer extends AbstractTransformer
{
    public function transform(UserFavorite $favorite)
    {
        return [
            'id' => $favorite->cat_id
        ];
    }
}
