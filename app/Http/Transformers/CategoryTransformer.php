<?php

namespace AOD\Http\Transformers;

use League\Fractal\TransformerAbstract;
use stdClass;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * Check out the schema section in the link below for available properties
     * @param stdClass $category
     * @link https://docs.thecatapi.com/api-reference/categories/categories-list
     * @return array
     */
    public function transform($category)
    {
        return [
            'id' => $category->id,
            'slug' => $category->name,
            'name' => ucfirst($category->name),
            'link' => url_for('api-cat-by-category', [
                'category' => $category->id
            ])
        ];
    }
}
