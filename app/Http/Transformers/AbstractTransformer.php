<?php

namespace AOD\Http\Transformers;

use League\Fractal\Manager;
use League\Fractal\TransformerAbstract as BaseTransformerAbstract;

abstract class AbstractTransformer extends BaseTransformerAbstract
{
    /**
     * @var Manager
     */
    protected $fractal;

    /**
     * @var array
     */
    protected $params;

    public function __construct($params = [])
    {
        $this->params = $params;
    }
}
