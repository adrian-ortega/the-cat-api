<?php

namespace AOD\Http\Transformers;

use stdClass;

class CatTransformer extends AbstractTransformer
{
    protected $defaultIncludes = [
        'categories',
        'breeds'
    ];

    protected $availableIncludes = [
        'favorite_id'
    ];

    /**
     * Check out the schema section in the link below for available properties
     * @param stdClass $cat
     * @link https://docs.thecatapi.com/api-reference/images/images-search
     * @return array
     */
    public function transform($cat)
    {
        return [
            'id' => data_get($cat, 'image.id', data_get($cat, 'id')),
            'url' => data_get($cat, 'image.url', data_get($cat, 'url')),
            'width' => data_get($cat, 'width'),
            'height' => data_get($cat, 'height')
        ];
    }

    public function includeBreeds($cat)
    {
        return $this->collection(
            data_get($cat, 'breeds', []),
            new BreedTransformer()
        );
    }

    public function includeCategories($cat)
    {
        return $this->collection(
            data_get($cat, 'categories', []),
            new CategoryTransformer()
        );
    }

    public function includeFavoriteId($favoritedCat)
    {
        return $this->primitive(data_get($favoritedCat, 'id'));
    }
}
