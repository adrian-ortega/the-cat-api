<?php

namespace AOD\Http\Transformers;

use AOD\Models\User;

class UserTransformer extends AbstractTransformer
{
    protected $availableIncludes = [
        'favorites',
        'favorite_count'
    ];

    protected $defaultIncludes = [
        'favorites',
    ];

    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name
        ];
    }

    public function includeFavorites(User $user)
    {
        return $this->collection($user->favorites, new UserFavoriteTransformer());
    }

    public function includeFavoriteCount(User $user)
    {
        return $this->primitive($user->favorites->count());
    }
}
