<?php

namespace AOD\Http\Transformers;

use stdClass;

class BreedTransformer extends AbstractTransformer
{
    /**
     * Check out the schema section in the link below for available properties
     * @param stdClass $breed
     * @link https://docs.thecatapi.com/api-reference/breeds/breeds-list
     * @return array
     */
    public function transform($breed)
    {
        return [
            'id' => $breed->id,
            'name' => $breed->name,
            'description' => $breed->description
        ];
    }
}
