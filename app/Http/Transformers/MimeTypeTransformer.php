<?php

namespace AOD\Http\Transformers;

class MimeTypeTransformer extends AbstractTransformer
{
    /**
     * @param string $mimeType
     * @return array
     */
    public function transform($mimeType)
    {
        return [
            'type' => $mimeType,
            'name' => strtoupper($mimeType)
        ];
    }
}
