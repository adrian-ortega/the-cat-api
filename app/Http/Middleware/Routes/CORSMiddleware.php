<?php

namespace AOD\Http\Middleware\Routes;

use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class CORSMiddleware
{
    /**
     * @var bool
     */
    protected $debug;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var array
     */
    protected $options = [
        'origin' => '*',
        'maxAge' => 1728000,
        'credentials' => true,
        'methods' => ['GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'OPTIONS'],
        'headers' => [
            'X-Requested-With',
            'Content-Type',
            'Accept',
            'Origin',
            'Authorization',
        ],
    ];

    public function __construct()
    {
        $this->debug = getenv('APP_DEBUG');
    }

    public function __invoke(Request $request, RequestHandler $handler)
    {
        $this->response = $handler->handle($request);
        $this->request = $request;

        $this->setOrigin();
        $this->setAllowHeaders();
        $this->setAllowMethods();
        $this->setAllowCredentials();

        if ($this->request->getMethod() === 'options') {
            $this->setMaxAge();
        }

        return $this->response;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Private and Protected

    /**
     * Sets origin based on the request
     */
    protected function setOrigin()
    {
        $origin = $this->debug ? $this->options['origin'] : site_url();

        if (is_array($origin)) {
            $allowedOrigins = $origin;
            $origin = reset($allowedOrigins);

            foreach ($allowedOrigins as $allowedOrigin) {
                if ($allowedOrigin === $this->request->getHeader('Origin')) {
                    $origin = $allowedOrigin;
                    break;
                }
            }
        }

        $this->response = $this->response->withHeader('Access-Control-Allow-Origin', $origin);
    }

    /**
     * Sets the allowed headers
     */
    protected function setAllowHeaders()
    {
        $allowHeaders = isset($this->options['headers']) ? $this->options['headers'] : false;

        if (!$allowHeaders) {
            $allowHeaders = $this->request->getHeader('Access-Control-Request-Headers');
        }

        if (is_array($allowHeaders)) {
            $allowHeaders = implode(', ', $allowHeaders);
        }

        $this->response = $this->response->withHeader('Access-Control-Allow-Headers', $allowHeaders);
    }

    /**
     * Set max age (default is 48 hours)
     */
    protected function setMaxAge()
    {
        if (isset($this->options['maxAge'])) {
            $this->response->withHeader('Access-Control-Max-Age', $this->options['maxAge']);
        }
    }

    /**
     * Allow credentials header (max is false)
     */
    protected function setAllowCredentials()
    {
        if (isset($this->options['allowCredentials']) && $this->options['allowCredentials'] === true) {
            $this->response = $this->response->withHeader('Access-Control-Allow-Credentials', true);
        }
    }

    /**
     * Allowed methods
     */
    protected function setAllowMethods()
    {
        if (isset($this->options['methods'])) {
            $methods = implode(', ', (array)$this->options['methods']);
            $this->response = $this->response->withHeader('Access-Control-Allow-Methods', $methods);
        }
    }
}
