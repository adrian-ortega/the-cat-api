<?php

namespace AOD\Http\Middleware;

use League\Container\ContainerAwareInterface;
use League\Container\ContainerAwareTrait;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Request;

class RequestCaptureMiddleware implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function __invoke(Request $request, RequestHandler $handler)
    {
        $this->getContainer()->add(Request::class, $request);

        return $handler->handle($request);
    }
}
