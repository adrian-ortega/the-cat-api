<?php

namespace AOD\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

/**
 * Class UserFavorite
 * @package AOD\Models
 * @property int $user_id
 * @property string $cat_id
 */
class UserFavorite extends BaseModel
{
    protected $fillable = [
        'user_id',
        'cat_id',
        'favorite_id'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
