<?php

namespace AOD\Models;

use Cartalyst\Sentinel\Users\EloquentUser as BaseModel;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class User
 * @package AOD\Models
 * @property int $id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property HasMany $favorites
 */
class User extends BaseModel
{
    /**
     * @return HasMany
     */
    public function favorites()
    {
        return $this->hasMany(UserFavorite::class, 'user_id');
    }
}
