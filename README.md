# The Cat API app

![Loader Cat](https://catapi.ortegas.co/images/loader-cat.gif)

## Getting started

This app is built using [Slim 4](http://www.slimframework.com/) and relies on
[composer](https://getcomposer.org/) as the package manager. The front end
of the site is built using [Vue](https://vuejs.org/), [Vuex](https://vuex.vuejs.org/)
and the typical webpack, node and es6 style modules.

You'll need to host the backend, if you don't plan to, skip to the Frontend section.

### The backend
After cloning down the app, you'll have to run composer to include the 
vendor files:

```bash
composer install
```

If you have an apache server set up and would like to run the app locally,
point it to the `/public` folder. Otherwise, the backend of the app is 
already hosted at [catapi.ortegas.co](catapi.ortegas.co).

#### DB and ENV
The backend of the app uses a `.env` file for configuration. After running
composer, the `.env` file should have copied over from `.env.example` but 
you will need to update it to get it pointed to your Database.

```
DB_DRIVER=mysql
DB_HOST=localhost
DB_NAME=thecatapi
DB_USER=root
DB_PASSWORD=root
```

**Note:** The database and it's tables will install itself. All you need to do
is correctly point it to the database and it will do the rest.

#### The Cat API key
Same as above, the API key lives in the `.env` file. You can get one for
free at [thecatapi.com](https://thecatapi.com/signup).

```
THE_CAT_API_KEY=your-awesome-secret-key
```

### The Frontend

The frontend is straight forward and is pointed to the hosted backend. 
Technically you only need to run the frontend.
```bash
1. CD into `/resources/vue`
2. Install modules

$ npm install

3. Run the development script and serve the app

$ npm run start

4. Go to http://localhost:8080
```

