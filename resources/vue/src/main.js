import './styles/app.scss'

import Vue from 'vue'
import App from './App.vue'
import store from './store'
import PortalVue from 'portal-vue'

store.dispatch('auth/check')

Vue.config.productionTip = false
Vue.use(PortalVue)

new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app')
