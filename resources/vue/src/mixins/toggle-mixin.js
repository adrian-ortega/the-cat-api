export default {
  data () {
    return {
      isOpen: false,
    }
  },
  methods: {
    toggle () {
      this.isOpen = !this.isOpen
      this.$emit(this.isOpen ? 'open' : 'close')
    },
    open () {
      this.isOpen = true
      this.$emit('open')
    },
    close () {
      this.isOpen = false
      this.$emit('close')
    },
    outsideClickEventHandler (event) {
      if (!this.isOpen) {
        return false
      }

      const $el = this.$refs['toggleRoot'] ? this.$refs['toggleRoot'] : this.$el
      if (event.target !== $el && !$el.contains(event.target)) {
        this.close()
      }
    },
    escapeKeyEventHandler (event) {
      if (event.keyCode === 27) {
        this.close()
      }
    }
  },
  watch: {
    isOpen (value) {
      if (value) {
        document.addEventListener('click', this.outsideClickEventHandler, false)
        document.addEventListener('keydown', this.escapeKeyEventHandler, false)
      } else {
        document.removeEventListener('click', this.outsideClickEventHandler, false)
        document.removeEventListener('keydown', this.escapeKeyEventHandler, false)
      }
    }
  }
}
