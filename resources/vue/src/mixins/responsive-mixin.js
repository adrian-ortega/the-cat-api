import {isDesktop, isMobile, isTouch} from '../util'

export default {
  data () {
    return {
      breakpoints: {
        mobile: true,
        touch: false,
        desktop: false
      }
    }
  },
  computed: {
    isMobile () {
      return this.breakpoints.mobile
    },
    isTouch () {
      return this.breakpoints.touch
    },
    isDesktop () {
      return this.breakpoints.desktop
    }
  },
  methods: {
    responsiveResize () {
      this.breakpoints = {
        mobile: isMobile(),
        touch: isTouch(),
        desktop: isDesktop()
      }
    },
    addResponsiveWindowEvents () {
      window.addEventListener('resize', () => this.responsiveResize())
      setTimeout(() => this.responsiveResize(), 10)
    }
  },
  mounted () {
    this.addResponsiveWindowEvents()
  }
}
