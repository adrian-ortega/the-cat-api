import {mapActions, mapGetters} from 'vuex'

export default {
  computed: {
    ...mapGetters({
      likedItems: 'auth/favorites'
    }),
    liked () {

      return this.likedItems ? this.likedItems.includes(this.cat.id) : false
    }
  },
  methods: {
    ...mapActions({
      likeItem: 'auth/favoriteItem',
      unlikeItem: 'auth/removeFavorite'
    }),
    like () {
      this.likeItem(this.cat.id)
    },
    toggleLiked () {
      return this.liked
        ? this.unlikeItem(this.cat.id)
        : this.like()
    },
  }
}
