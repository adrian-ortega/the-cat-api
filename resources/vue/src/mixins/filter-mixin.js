import {mapGetters, mapMutations} from 'vuex'
export default {
  data () {
    return {
      filterKey: null,
      value: null
    }
  },
  computed: {
    ...mapGetters({
      filters: 'cats/filters',
    })
  },
  methods: {
    ...mapMutations({
      SET_FILTERS: 'cats/SET_FILTERS'
    }),
    updateSelected (value) {
      const filters = {...this.filters}

      if(this.filterKey) {
        filters[this.filterKey] = value
      }

      this.value = value
      this.SET_FILTERS(filters)
      this.$emit('input', filters)
    },
    updateAndClose (value, close) {
      this.updateSelected(value)
      close()
    }
  }
}
