export const user = (state) => state.user

export const authenticated = (state) => state.authenticated

export const favorites = (state) => {
  if(!state.authenticated) {
    return []
  }

  return state.user.favorites
}
