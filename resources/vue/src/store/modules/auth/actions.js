import api from '../../../api'

const JWT_TOKEN_KEY = '__cat_app_token'

const _authenticate = (commit, user, token) => {
  localStorage.setItem(JWT_TOKEN_KEY, token)
  commit('SET_USER', user)
  commit('SET_AUTHENTICATION', true)
}

const _getToken = () => localStorage.getItem(JWT_TOKEN_KEY)

export const check = ({commit}) => {
  const token = _getToken()

  if(token) {
    return api.auth.me(token).then(({user, meta}) => {
      _authenticate(commit, user, meta.token)
    }).catch(() => {
      // return logout({commit})
    })
  }

  return false
}

export const login = ({commit}, credentials) => {
  return api.auth.login(credentials).then(({user, meta}) => {
    _authenticate(commit, user, meta.token)
  })
}

export const register = ({commit}, credentials) => {
  return api.auth.register(credentials).then(({user, meta}) =>{
    _authenticate(commit, user, meta.token)
  })
}

export const logout = ({commit}) => {
  return api.auth.logout().then(() => {
    localStorage.removeItem(JWT_TOKEN_KEY)
    commit('SET_USER', null)
    commit('SET_AUTHENTICATION', false)
  })
}

export const favoriteItem = ({commit}, id) => {
  const token = _getToken()
  if(token) {
    return api.auth.favorite(token, id).then(({data}) => {
      commit('SET_FAVORITES', data)
    })
  }

  return null
}

export const removeFavorite = ({commit}, id) => {
  const token = _getToken()
  if(token) {
    return api.auth.unfavorite(token, id).then(({data}) => {
      commit('SET_FAVORITES', data)
    })
  }

  return null
}

export const getFavorites = () => {
  const token = _getToken()
  if(token) {
    return api.auth.favorites(token).then(({data, meta}) => {
      return {
        data: data,
        pagination: meta.pagination
      }
    })
  }

  return null
}
