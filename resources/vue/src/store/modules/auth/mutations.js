export const SET_USER = (state, user) => {
  state.user = user
}

export const SET_AUTHENTICATION = (state, authenticated) => {
  state.authenticated = authenticated
}

export const SET_FAVORITES = (state, favorites) => {
  state.user.favorites = favorites
}
