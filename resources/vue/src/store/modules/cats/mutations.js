import {LOCAL_STORAGE_KEY} from './config'

/**
 * @param {Object} state
 * @param {Boolean} loading
 * @constructor
 */
export const SET_LOADING = (state, loading) => {
  state.loading = loading
}

/**
 * @param {Object} state
 * @param {Array} items
 * @constructor
 */

export const SET_ITEMS = (state, items) => {
  state.items = items
}

/**
 * @param {Object} state
 * @param {Array} likedItems
 * @constructor
 */
export const SET_LIKED_ITEMS = (state, likedItems) => {
  localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(likedItems))
  state.likedItems = likedItems
}

/**
 * @param {Object} state
 * @param {Object} pagination
 * @constructor
 */
export const SET_PAGINATION = (state, pagination) => {
  state.pagination = pagination
}

/**
 * @param {Object} state
 * @param {Number} page
 * @constructor
 */
export const SET_PAGE = (state, page) => {
  state.pagination.page = page
}

/**
 * @param {Object} state
 * @param {Object} filters
 * @constructor
 */
export const SET_FILTERS = (state, filters) => {
  state.filters = filters
}
