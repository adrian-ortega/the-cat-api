export const loading = (state) => state.loading

/**
 * @param {Object} state
 * @returns {Array|Object[]}
 */
export const items = (state) => state.items

/**
 * @param {Object} state
 * @returns {Array|String[]}
 */
export const likedItems = (state) => state.likedItems

/**
 * @param {Object} state
 * @returns {{total: number, limit: number, page: number}}
 */
export const pagination = (state) => state.pagination

/**
 * @param {Object} state
 * @returns {string|{[p: string]: Function}|Object|{category: number, type: number}}
 */
export const filters = (state) => state.filters
