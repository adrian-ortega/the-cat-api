export default {
  loading: true,
  items: [],
  likedItems: [],
  filters: {
    category: -1,
    type: -1
  },
  pagination: {
    page: 1,
    limit: 9,
    total: 1
  }
}
