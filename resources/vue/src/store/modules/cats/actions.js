import api from '../../../api'
import {LOCAL_STORAGE_KEY} from './config'

export const clearCats = ({commit}) => {
  commit('SET_LOADING', true)
  return new Promise((resolve) => {
    commit('SET_ITEMS', [])
    commit('SET_PAGINATION', {
      page: 1,
      limit: 9,
      total: 1
    })
    commit('SET_LOADING', false)
    resolve()
  })
}

export const getCats = ({commit, getters}) => {
  commit('SET_LOADING', true)
  const page = getters.pagination.page
  const limit = getters.pagination.limit

  const category = getters.filters.category
  const type = getters.filters.type

  return api.cats.getCats({page, limit, category, type})
    .then(({data, pagination}) => {
      commit('SET_ITEMS', data)
      commit('SET_PAGINATION', pagination)
      commit('SET_LOADING', false)
    })
}

const filterLiked = (stored, itemId) => stored.slice().filter(storedItemId => storedItemId !== itemId)

export const likeItem = ({commit, getters}, itemId) => {
  const likedItems = filterLiked(getters.likedItems)
  likedItems.push(itemId)
  commit('SET_LIKED_ITEMS', likedItems)
}

export const unlikeItem = ({commit, getters}, itemId) => {
  commit('SET_LIKED_ITEMS', filterLiked(getters.likedItems, itemId))
}

export const loadStoredLikes = ({commit}) => {
  const likes = localStorage.getItem(LOCAL_STORAGE_KEY)
  if(likes) {
    commit('SET_LIKED_ITEMS', JSON.parse(likes))
  }
}
