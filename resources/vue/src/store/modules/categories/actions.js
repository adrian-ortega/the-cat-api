import api from '../../../api'

export const getCategories = ({commit}) => {
  return api.categories.getCategories().then(({data}) => {
    commit('SET_CATEGORIES', data)
  })
}
