import auth from './auth'
import cats from './cats'
import categories from './categories'

export default {
  auth,
  cats,
  categories,
}
