import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    view: 'default'
  },
  getters: {
    view: state => state.view
  },
  mutations: {
    UPDATE_VIEW: (state, view) => {
      state.view = view
    }
  },
  modules
})
