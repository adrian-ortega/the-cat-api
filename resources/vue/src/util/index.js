export const randomIdFromString = (str) => {
  const s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1)
  }

  let randomId = ''
  for (let i = 0; i < 4; i++) {
    randomId += s4()
  }
  return str.length > 0 ? `${str}-${randomId}` : randomId
}

/**
 * @param {Object} obj
 * @return {FormData}
 */
export const formDataParams = (obj = {}) => {
  let params = new FormData()

  Object.entries(obj).forEach(([key, value]) => {
    params.set(key, value)
  })

  return params
}

/**
 * Formats a number with commas
 * @param {Number} num
 * @returns {string}
 */
export const formatNumber = (num) => num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

export const range = (start= 0, end) => {
  return Array.from(
    Array(Math.abs(end - start)),
    (_, i) => start + i
  )
}

export const BREAKPOINT_GAP = 32
export const BREAKPOINT_TABLET = 769
export const BREAKPOINT_DESKTOP = 960 + (BREAKPOINT_GAP * 2)
export const BREAKPOINT_WIDESCREEN = 1152 + (BREAKPOINT_GAP * 2)
export const BREAKPOINT_FULLHD = 1344 + (BREAKPOINT_GAP * 2)

export const isMobile = () => window.innerWidth < BREAKPOINT_TABLET

export const isTouch = () => window.innerWidth < BREAKPOINT_DESKTOP

export const isDesktop = () => window.innerWidth > BREAKPOINT_DESKTOP

export const isWidescreen = () => window.innerWidth > BREAKPOINT_WIDESCREEN

export const isFullHD = () => window.innerWidth > BREAKPOINT_FULLHD
