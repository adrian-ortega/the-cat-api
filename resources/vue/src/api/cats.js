import axios from 'axios'
import {API_ENDPOINTS} from './config'

/**
 * @param params
 * @returns {Promise<AxiosResponse<T>>}
 */
export const getCats = (params = {}) => {


  return axios.get(API_ENDPOINTS.cats, {params}).then(({data}) => {
    return {
      data: data.data,
      pagination: data.meta.pagination
    }
  })
}
