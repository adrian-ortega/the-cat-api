import axios from 'axios'
import {API_ENDPOINTS} from './config'

/**
 * @returns {Promise<{data: *}>}
 */
export const getCategories = () => {
  return axios.get(API_ENDPOINTS.categories).then(({data}) => {
    return {
      data: data.data
    }
  })
}
