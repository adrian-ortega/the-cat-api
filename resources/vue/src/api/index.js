import * as categories from './categories'
import * as cats from './cats'
import * as auth from './auth'

export default {
  auth,
  categories,
  cats
}
