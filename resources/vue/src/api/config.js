export const API_BASE_URL = 'https://catapi.ortegas.co/api'

export const API_ENDPOINTS = {
  auth: `${API_BASE_URL}/auth`,
  cats: `${API_BASE_URL}/cats`,
  categories: `${API_BASE_URL}/cats/categories`,
  user: `${API_BASE_URL}/user`
}
