import axios from 'axios'
import {API_ENDPOINTS} from './config'
import {formDataParams} from '../util'

const flattenFavorites = favorites => favorites.map(u => u.id)

const transformUser = user => {
  user.favorites = flattenFavorites(user.favorites.data)
  return user
}

export const login = ({email, password, persist}) => {
  const params = formDataParams({email, password, persist})
  return axios.post(`${API_ENDPOINTS.auth}/login`, params)
    .then(({data}) => {
      return {
        user: transformUser(data.data),
        meta: data.meta
      }
    })
}

export const logout = () => {
  return axios.get(`${API_ENDPOINTS.auth}/logout`)
}

export const register = ({email, password, first_name, last_name}) => {
  const params = formDataParams({email, password, first_name, last_name})
  return axios.post(`${API_ENDPOINTS.auth}/register`, params)
    .then(({data}) => {
      return {
        user: transformUser(data.data),
        meta: data.meta
      }
    })
}

const withAuthParams = (token, params = null) => ({
  headers: {
    Authorization: `Bearer ${token}`
  },
  params
})

export const me = (token) => {
  return axios.get(`${API_ENDPOINTS.user}/me`, withAuthParams(token)).then(({data}) => {
    return {
      user: transformUser(data.data),
      meta: data.meta
    }
  })
}

export const favorites = (token) => {
  return axios.get(`${API_ENDPOINTS.user}/favorites`, withAuthParams(token)).then(({data}) => {
    return data
  })
}

export const favorite = (token, id) => {
  return axios.post(
    `${API_ENDPOINTS.user}/favorites`,
    formDataParams({id}),
    withAuthParams(token)
  ).then(({data}) => {
    return {
      data: flattenFavorites(data.data)
    }
  })
}

export const unfavorite = (token, id) => {
  return axios.delete(
    `${API_ENDPOINTS.user}/favorites`,
    withAuthParams(token, {id})
  ).then(({data}) => {
    return {
      data: flattenFavorites(data.data),
      meta: data.meta
    }
  })
}
