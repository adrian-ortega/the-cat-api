<?php
/**
 * Web accessible routes
 * @var Slim\App $app
 */

use AOD\Http\Controllers\HomeController;

$app->get('/', HomeController::class . ':index')->setName('home');
