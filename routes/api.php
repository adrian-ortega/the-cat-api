<?php
/**
 * Web accessible routes
 * @var Slim\App $app
 */

use AOD\Http\Controllers\Api\Auth\LoginController;
use AOD\Http\Controllers\Api\Auth\LogoutController;
use AOD\Http\Controllers\Api\Auth\RegisterController;
use AOD\Http\Controllers\Api\Auth\TokenController;
use AOD\Http\Controllers\Api\CategoriesResourceController;
use AOD\Http\Controllers\Api\CatsResourceController;
use AOD\Http\Controllers\Api\ForbiddenController;
use AOD\Http\Controllers\Api\NotFoundController;
use AOD\Http\Controllers\Api\TypesResourceController;
use AOD\Http\Controllers\Api\UserController;
use AOD\Http\Middleware\Routes\CORSMiddleware;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Slim\Routing\RouteCollectorProxy;

$app->group('/api', function (RouteCollectorProxy $group) {
    $group->get('', ForbiddenController::class)->setName('api-index');
    $group->get('/cats', CatsResourceController::class . ':index')->setName('api-cat-index');

    $group->get('/cats/categories', CategoriesResourceController::class . ':index')->setName('api-cat-categories');
    $group->get('/cats/categories/{category}', CatsResourceController::class . ':byCategory')->setName('api-cat-by-category');

    $group->get('/cats/types', TypesResourceController::class . ':index')->setName('api-cat-types');
    $group->get('/cats/types/{type}', CatsResourceController::class . ':byType')->setName('api-cat-by-type');

    $group->get('/auth', function(Request $request, Response $response) {
        return $response->withHeader('Location', url_for('home'));
    });

    $group->get('/auth/register', RegisterController::class . ':index')->setName('api-auth-register');
    $group->post('/auth/register', RegisterController::class . ':action');

    $group->get('/auth/login', LoginController::class . ':index')->setName('api-auth-login');
    $group->post('/auth/login', LoginController::class . ':action');

    $group->map(['GET', 'POST'], '/auth/logout', LogoutController::class . ':index')->setName('api-auth-logout');

    $group->get('/user/me', UserController::class . ':me')->setName('api-user-me');
    $group->get('/user/favorites', UserController::class . ':favorites')->setName('api-user-favorites');
    $group->post('/user/favorites', UserController::class . ':addFavorite');
    $group->delete('/user/favorites', UserController::class . ':removeFavorite');

    $group->get('/not-found', NotFoundController::class)->setName('api-not-found');

    $group->options('/{routes:.+}', function (Request $request, Response $response) {
        $response->getBody()->write(json_encode(['pre_flight' => 'ok']));
        $responseWithJson = $response->withHeader('Content-Type', 'application/json');
        return $responseWithJson->withStatus(200);
    });
});

    //->add(new CORSMiddleware());
