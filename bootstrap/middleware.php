<?php
/**
 * Middleware is used throughout the application to intervene requests
 * and either stop them from running if rules are not met, or add
 * functionality to the request depending on path and parameters.
 *
 * @var Slim\App $app
 */

use AOD\Exceptions\ExceptionHandler;
use AOD\Http\Middleware\RequestCaptureMiddleware;
use AOD\Models\User;
use Slim\Psr7\Request;
use Tuupola\Middleware\JwtAuthentication;

// Adds the request object to the container
$app->add(RequestCaptureMiddleware::class);

// Adds JSON Web Token authentication to routes under /api/user
$app->add(new JwtAuthentication([
    'path' => [
        '/api/user'
    ],

    // Can be changed if under SSL, or use an ENV
    'secure' => false,

    'secret' => getenv('JWT_SECRET'),
    'relaxed' => ['localhost', 'localhost:8080'],
    'before' => function(Request $request, $arguments) {
        return $request->withAttribute('user', User::find(data_get($arguments, 'decoded.user')));
    }
]));

// Adds Cross Origin Resource Sharing rules. This is a simple app, so we allow it all
$app->addMiddleware(new Tuupola\Middleware\CorsMiddleware([
    'origin' => ['*'],
    'methods' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
    'headers.allow' => ['*'],
    'headers.expose' => ['Etag'],
    'credentials' => true,
    'cache' => 86400
]));

// Intervenes exception handling when a validation exception is thrown
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
$errorMiddleware->setDefaultErrorHandler(new ExceptionHandler());
