<?php

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Native\SentinelBootstrapper;
use League\Container\Container;
use League\Container\ContainerAwareInterface;
use League\Container\ReflectionContainer;
use Slim\Factory\AppFactory;

// Paths to be used throughout the app
if(!defined('DS'))          { define('DS', DIRECTORY_SEPARATOR); }
if(!defined('ABS_PATH'))    { define('ABS_PATH', realpath(__DIR__ . '/../') . DS); }
if(!defined('PUBLIC_PATH')) { define('PUBLIC_PATH', realpath(__DIR__ . '/../../public') . DS); }
if(!defined('BOOT_PATH'))   { define('BOOT_PATH', ABS_PATH . 'bootstrap' . DS); }
if(!defined('APP_PATH'))    { define('APP_PATH', ABS_PATH . 'app' . DS); }

$autoloader = ABS_PATH . 'vendor' . DS . 'autoload.php';

// Check to see if we've created the autoloader file in the composer vendor dir
if(!file_exists($autoloader)) {
    echo '<h1>Can\'t find vendor/autoload.php. </h1>';
    echo '<h3>Please install all required composer vendor dependencies</h3>';
    die();
}

require_once $autoloader;

try {
    Dotenv\Dotenv::createImmutable(ABS_PATH)->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    // There's no real need to report this, this app isn't big enough.
}

if (getenv('APP_DEBUG')) {

    // Here we can include such things as the Filp/Whoops package, but at bare minimum, we turn error
    // reporting on.
    //
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    // The debug file can be deleted for production
    if(file_exists($_debugFilePath = BOOT_PATH . '/debug.php')) {
        require_once $_debugFilePath;
    }
}

// Boot database
require_once BOOT_PATH . 'database.php';

// Boot Auth
$sentinelConfig = require(ABS_PATH . 'config/auth.php');
Sentinel::instance(new SentinelBootstrapper($sentinelConfig));

// Boot Container
$container = new Container();
$container->delegate(new ReflectionContainer);
$container->inflector(ContainerAwareInterface::class)->invokeMethod('setContainer', [$container]);

// Boot App
AppFactory::setContainer($container);
$app = AppFactory::create();

// Load providers
require_once BOOT_PATH . 'providers.php';

// Load middleware
require_once BOOT_PATH . 'middleware.php';

// Load routes
foreach (['api', 'web'] as $routeFile) {
    $filePath = sprintf('%s/%s.php', ABS_PATH . '/routes', $routeFile);
    if(file_exists($filePath)) {
        require $filePath;
    }
}
