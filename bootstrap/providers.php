<?php
/**
 * Since the application uses containers and dependency injections,
 * we need to provide certain services to share throughout. This
 * file adds these provider classes to the container so we know
 * where to look when we request certain things like the CAT API
 * object.
 *
 * @var App $app
 * @var Container $container
 */

use AOD\Providers\RoutesServiceProvider;
use AOD\Providers\TheCatsApiServiceProvider;
use AOD\Providers\TwigViewServiceProvider;
use AOD\Providers\ValidationServiceProvider;
use League\Container\Container;
use Slim\App;

// Routes
$container->addServiceProvider(new RoutesServiceProvider($app));

// Validation rules
$container->addServiceProvider(ValidationServiceProvider::class);

// Twig
$container->addServiceProvider(TwigViewServiceProvider::class);

// The Cats
$container->addServiceProvider(TheCatsApiServiceProvider::class);
