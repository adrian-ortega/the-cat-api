<?php
//
// This file can be removed for prod
//

if (class_exists('Whoops\\Run')) {
    $whoops = new \Whoops\Run;
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    $whoops->register();
}
