<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

$capsule = new Capsule();

$capsule->addConnection([
    'driver' => getenv('DB_DRIVER'),
    'host' => getenv('DB_HOST'),
    'database' => getenv('DB_NAME'),
    'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci'
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DATABASE INSTALL

$schema = $capsule->schema('default');

// -- Users -------------------------------------------------------

if (!$schema->hasTable('users')) {
    $schema->create('users', function (Blueprint $table) {
        $table->increments('id');
        $table->string('email')->unique();
        $table->string('password');
        $table->timestamp('last_login')->nullable();;
        $table->string('first_name');
        $table->string('last_name');
        $table->timestamps();
    });
}

// -- Activations -------------------------------------------------

if (!$schema->hasTable('activations')) {
    $schema->create('activations', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('user_id');
        $table->string('code');
        $table->boolean('completed')->default(false);
        $table->timestamp('completed_at')->nullable();;
        $table->timestamps();

        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
    });
}

// -- Persistences ------------------------------------------------

if (!$schema->hasTable('persistences')) {
    $schema->create('persistences', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('user_id');
        $table->string('code');
        $table->timestamps();

        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
    });
}

// -- Throttle ----------------------------------------------------

if(!$schema->hasTable('throttle')) {
    $schema->create('throttle', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('user_id');
        $table->string('type');
        $table->string('ip');
        $table->timestamps();
    });
}

// -- Favorites/Likes ---------------------------------------------

if(!$schema->hasTable('user_favorites')) {
    $schema->create('user_favorites', function (Blueprint $table) {
        $table->integer('user_id')->unsigned();
        $table->string('cat_id');

        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
    });
}

// -- Update Favorites ---------------------------------------------

if(!$schema->hasColumn('user_favorites', 'favorite_id')) {
    $schema->table('user_favorites', function (Blueprint $table) {
        $table->integer('favorite_id');
    });
}
